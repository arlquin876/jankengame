package Model;

public class Janken {
	public String myHand; //自分の手
	public String pcHand; //相手の手
	public String result; //勝負結果
	public int win; 	  //勝ちの数
	public int lose;	  //負けの数


	public String getMyHand() {
		return myHand;
	}
	public void setMyHand(String myHand) {
		this.myHand = myHand;
	}
	public String getPcHand() {
		return pcHand;
	}
	public void setPcHand(String pcHand) {
		this.pcHand = pcHand;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getWin() {
		return win;
	}
	public void setWin(int win) {
		this.win = win;
	}
	public int getLose() {
		return lose;
	}
	public void setLose(int lose) {
		this.lose = lose;
	}


}
