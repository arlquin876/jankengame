package Model;

public class JankenLogic {
	public void excute(Janken janken, String hand){
		//自分の手
		janken.setMyHand(hand);

		// 手を配列に設定
		String[] hands = {"グー","チョキ","パー"};

		// PCの手をランダムで決める
		janken.setPcHand(hands[(int)(Math.random() * hand.length())]);

		//勝敗ロジック
		if(janken.getMyHand().equals(janken.getPcHand())){
			janken.setResult("引き分け");
		} else if(janken.getMyHand().equals("グー") && janken.getPcHand().equals("チョキ")
				|| janken.getMyHand().equals("チョキ") && janken.getPcHand().equals("パー")
				|| janken.getMyHand().equals("パー") && janken.getPcHand().equals("グー")){
			janken.setResult("あなたの勝ちです。");
			janken.setWin(janken.getWin()+1);
		} else {
			janken.setResult("あなたの負けです。");
			janken.setLose(janken.getLose()+1);
		}
	}
}
