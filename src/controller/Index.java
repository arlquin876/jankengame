package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Janken;
import Model.JankenLogic;
/**
 * Servlet implementation class index
 */
@WebServlet("/index.html")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメータを取得する
		String action = (String)request.getParameter("action");
		if(action != null && action.equals("reset")){
			//セッションの取得
			HttpSession session = request.getSession();
			//セッションの削除
			session.removeAttribute("janken");
			//リダイレクト
			response.sendRedirect("/JankenGame/");
		}else{
			//フォワードへの処理
			RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//リクエストパラメータを取得する
		String hand = (String)request.getParameter("hand");
		if(hand == null) {
			//セッションの削除
			request.setAttribute("err", "手を選んでください");
			//フォワードへの処理
			RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
			return;
		}
		//セッションの取得
		HttpSession session = request.getSession();
		//セッションの保存
		Janken janken = (Janken)session.getAttribute("janken");
		if(janken == null) {
			janken = new Janken();
		}
		//JankenLogicのインスタンスの生成
		JankenLogic logic = new JankenLogic();
		logic.excute(janken, hand);
		session.setAttribute("janken", janken);
		doGet(request, response);
	}

}
