<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="Model.*"%>

<%
String err = (String)request.getAttribute("err");
Janken janken = (Janken)session.getAttribute("janken");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/style.css">
<title>じゃんけんゲーム</title>
</head>
<body>
<div class="game_title">
	<h1>じゃんけんゲーム</h1>
</div>
<%if(err != null) {%>
<p class="err_massege"><%=err %></p>
<%} %>

<!-- 手の選択 -->
<div  class="handChoices">
<form action="/JankenGame/index.html" method="post">
<img alt="グー" src="janken_image/s_0.png">
<input type="radio" id="gu" name="hand" value="グー">
<%if(janken !=null && janken.getMyHand().equals("グー")){} %>
 <label for="gu">グー</label>


<img alt="チョキ" src="janken_image/s_1.png">
<input type="radio" id="choki" name="hand" value="チョキ">
<%if(janken !=null && janken.getMyHand().equals("チョキ")){} %>
 <label for="choki">チョキ</label>

<img alt="パー" src="janken_image/s_2.png">
 <input type="radio" id="per" name="hand" value="パー">
<%if(janken !=null && janken.getMyHand().equals("パー")){} %>
 <label for="per">パー</label>

 <input type="submit" value="送信">
</form>
</div>

<%if(janken != null) {%>
<div class="hand_SelectResult">
	<!-- 自分が選んだ手の表示 -->
	<div class="myHand">
		<p>あなたは<%=janken.getMyHand() %>
		<%if(janken.getMyHand().equals("グー")){ %>
		<img alt="グー" src="janken_image/s_0.png">
		<%} else if(janken.getMyHand().equals("チョキ")){%>
		<img alt="チョキ" src="janken_image/s_1.png">
		<%} else {%>
		<img alt="パー" src="janken_image/s_2.png">
		<%} %>
		</p>
	</div>

	<!-- PCが選択した手の表示 -->
	<div class="pcHand">
		<p>PCは<%=janken.getPcHand() %>
		<%if(janken.getPcHand().equals("グー")) {%>
		<img alt="グー" src="janken_image/s_0.png">
		<%}else if(janken.getPcHand().equals("チョキ")) { %>
		<img alt="グー" src="janken_image/s_1.png">
		<%}else{ %>
		<img alt="グー" src="janken_image/s_2.png">
		<%} %>
		</p>
	</div>
</div>

<!-- 勝負結果 -->
<p><%=janken.result %></p>

<!-- 勝ち数のカウント -->
<p>Win:<%=janken.getWin() %></p>

<!-- 負け数のカウント -->
<p>Lose:<%=janken.getLose() %></p>
<a href="/JankenGame/index.html?action=reset" class="countRset">カウントをリセットする。</a>
<%} %>

</body>
</html>